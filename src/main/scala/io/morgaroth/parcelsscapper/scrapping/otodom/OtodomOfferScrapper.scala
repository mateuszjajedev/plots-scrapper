package io.morgaroth.parcelsscapper.scrapping.otodom

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import cats.syntax.option._
import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, NonActualOffer, OfferParsed, OfferParsing, OffererAI, RJsoup, RichClasses}
import io.morgaroth.parcelsscapper.errors.ScrapperError
import io.morgaroth.parcelsscapper.scrapping.DebugHelpers
import org.json4s.JsonAST.{JArray, JString}
import org.json4s.native.JsonMethods._
import org.json4s.{DefaultFormats, Formats}
import org.jsoup.Jsoup

case class OtodomFullOffer(phone: Option[String], offerer: Option[String], description: String, place: Option[String], addDate: LocalDate, area: Option[Long])

class OtodomOfferScrapper() extends CommonHtmlHelpers with RichClasses {
  implicit private val jsonFormats: Formats = DefaultFormats

  private val dateParser = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  def parseOfferUrl(url: String): OfferParsing[OtodomFullOffer] = {
    val doc = RJsoup.get(url).get
    try {
      parseOffer(doc)
    } catch {
      case e: ScrapperError =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, doc, url, e)
        throw e
      case e: NullPointerException =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, doc, url, e)
        throw e
    }
  }

  def readServerAppData(inputJson: String) = {
    val j = parse(inputJson)
    val phoneNumber = (j \ "initialProps" \\ "phones").foldField(List.empty[String]) {
      case (acc, (_, JArray(data))) =>
        data.map {
          case JString(s) => s
          case other => raise(s"WTF $other when JString expected with phone number")
        } ::: acc
    }.distinct
    val agency = (j \ "initialProps" \\ "agency" \ "name").extractOpt[String]
    val location = (j \ "initialProps" \\ "location" \ "address").extract[String]
    val dateAdded = LocalDate.parse((j \ "initialProps" \\ "dateCreated").extract[String], dateParser)
    val area = (j \ "initialProps" \ "meta" \ "target" \ "Area").extract[String].toDouble.toLong

    (phoneNumber, agency, location, dateAdded, area)
  }

  def parseOffer(str: String): OfferParsing[OtodomFullOffer] = {
    val doc = Jsoup.parse(str)

    val elements = doc.select("div#ad-not-available-box")
    if (elements.size() == 0) {
      val description = doc.selectSingle("section.section-description").text().toOneLineNormalized
      val place = doc.selectSingle("article").selectSingle("a[href=#map]").text().normalize.some
      val dataFromServerApp = readServerAppData(doc.selectSingle("script#server-app-state").data())
      val placeInterpreted = place.map { placeStr =>
        val placeParts = placeStr.split(",").map(_.trim)
        if (placeParts.length == 3) placeParts.head
        else if (placeParts.length == 4 && placeParts.take(2).distinct.length == 1) placeParts.head
        else if (placeParts.length == 4) placeParts.take(2).mkString(", ")
        else if (placeParts.length == 2) placeParts.mkString(", ")
        else raise(s"non-parsable place for otodom: '$placeStr'")
      }

      OfferParsed(OtodomFullOffer(
        phone = Option(dataFromServerApp._1).filter(_.nonEmpty).map(_.mkString(",")),
        offerer = dataFromServerApp._2.map(OffererAI.normalize),
        description = description,
        place = placeInterpreted,
        addDate = dataFromServerApp._4,
        area = Some(dataFromServerApp._5)
      ))
    } else NonActualOffer()
  }

}