package io.morgaroth.parcelsscapper.scrapping

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

import better.files.Dsl.pwd
import better.files.File.OpenOptions
import com.typesafe.scalalogging.LazyLogging

case object DebugHelpers extends LazyLogging {
  private val counters = new ConcurrentHashMap[Long, AtomicInteger]()

  def storeDebugDataForParsing(parserName: String, doc: String, url: String, exception: Throwable): Unit = {
    val tm = System.currentTimeMillis()
    val cnt = counters.getOrDefault(tm, new AtomicInteger())
    implicit val openOptions: OpenOptions = OpenOptions.append
    val file = pwd / s"$parserName-$tm-${cnt.getAndIncrement()}.html"
    file.write(url)
    file.write("\n")
    file.printWriter(autoFlush = true).map(exception.printStackTrace).get()
    file.write("\n\n")
    file.write(doc)
    logger.warn(s"error ${exception.getMessage} from $parserName stored in ${file.canonicalPath}")
  }
}