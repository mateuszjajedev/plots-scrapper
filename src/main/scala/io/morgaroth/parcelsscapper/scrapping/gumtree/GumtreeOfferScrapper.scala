package io.morgaroth.parcelsscapper.scrapping.gumtree

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import io.morgaroth.parcelsscapper.errors.ParsingAssertion
import io.morgaroth.parcelsscapper.scrapping.DebugHelpers
import org.jsoup.Jsoup


case class GumtreeFullOffer(id: String, phone: Option[String], place: String, title: Option[String], description: String, addDate: LocalDate, dead: Boolean)

class GumtreeOfferScrapper() extends CommonHtmlHelpers with RichClasses {

  private val dateParser = DateTimeFormatter.ofPattern("dd/MM/yyyy")

  def parseOfferUrl(url: String): GumtreeFullOffer = {
    val doc = RJsoup.get(url).get
    try {
      parseOffer(doc)
    } catch {
      case e: NullPointerException =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, doc, url, e)
        throw e
    }
  }

  def parseOffer(str: String): GumtreeFullOffer = {
    val doc = Jsoup.parse(str)
    val warn = doc.selectSingle("div.warning")
    val dead = Option(warn).map(_.text().normalize).exists(_.contains("wygasło"))
    val detailsPane = doc.select("div.vip-details")
    val selMenu = detailsPane.select("ul.selMenu")
    val addDate = selMenu.select("div:has(span:contains(Data dodania))").select("span.value").text()
    val place = selMenu.selectSingle("div.location").text().normalize
    val description = detailsPane.select("div.description").text().toOneLineNormalized
    val phone = Option(doc.selectSingle("a.telephone")).map(_.attr("href").stripPrefix("tel:"))
    val title = doc.selectSingle("span.myAdTitle").text().toOneLineNormalized
    val id = doc.selectSingle("input[id=adId]").attr("value")
    if (title.isEmpty) throw ParsingAssertion("Title is empty!")
    GumtreeFullOffer(id, phone, place, Some(title), description, LocalDate.parse(addDate, dateParser), dead)
  }
}