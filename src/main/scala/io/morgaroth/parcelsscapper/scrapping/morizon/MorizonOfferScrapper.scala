package io.morgaroth.parcelsscapper.scrapping.morizon

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import io.morgaroth.parcelsscapper.errors.ScrapperError
import io.morgaroth.parcelsscapper.scrapping.DebugHelpers
import org.jsoup.Jsoup


case class MorizonFullOffer(id: String, phone: String, place: String, description: String, area: Long, addDate: LocalDate, updateDate: LocalDate)

class MorizonOfferScrapper() extends CommonHtmlHelpers with RichClasses {

  private val dateParser = DateTimeFormatter.ofPattern("d MMMM yyyy")

  def parseOfferUrl(url: String): MorizonFullOffer = {
    val doc = RJsoup.get(url).get
    parseOffer(doc, url)
  }

  def parseOffer(html: String, url: String) = {
    try {
      unsafeParseOffer(html)
    } catch {
      case e: ScrapperError =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, html, url, e)
        throw e
      case e: NullPointerException =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, html, url, e)
        throw e
    }
  }

  def unsafeParseOffer(str: String): MorizonFullOffer = {
    val doc = Jsoup.parse(str)
    val id = doc.selectSingle("span.ucFavourite a").attr("data-id").trim.normalize

    val placesLine = doc.selectSingle("div.summaryLocation").selectSingle("div h1 strong").select("span").toVector.map(_.text.normalize.stripSuffix(","))
    val place = placesLine.length match {
      case 2 | 3 => placesLine.last
      case 4 => placesLine.drop(2).mkString(", ")
      case 5 => placesLine.drop(3).mkString(", ")
      case _ => raise(s"Unknown line $placesLine")
    }
    val phone = parsePhoneNumbers(doc.select("span[itemprop=telephone]").toVector.map(_.text).flatMap(_.split(";")))
    val updateDate = parseMorizonDate(doc.selectSingle("tr:contains(Zaktualizowano)").selectSingle("td").text().trim)
    val addDate = parseMorizonDate(doc.selectSingle("tr:contains(Opublikowano)").selectSingle("td").text().trim)
    val description = doc.selectSingle("section.descriptionContent").text().toOneLineNormalized
    val area = validateAndParseAreaInSqMeters(doc.selectSingle("li.paramIconLivingArea em").text().normalize)
    MorizonFullOffer(id, phone, place, description, area, addDate, updateDate)
  }

  private def parseMorizonDate(date: String) = date match {
    case "dzisiaj" => LocalDate.now()
    case "wczoraj" => LocalDate.now().minusDays(1)
    case _ => LocalDate.parse(date, dateParser)
  }
}