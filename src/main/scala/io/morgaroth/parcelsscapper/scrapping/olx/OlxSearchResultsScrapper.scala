package io.morgaroth.parcelsscapper.scrapping.olx

import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import org.jsoup.Jsoup
import org.jsoup.nodes.Element

case class OlxSearchEntry(id: String, price: Int, location: String, link: String)

case class OlxSearchPager(maxPages: Int, currentPage: Int, prevPage: Option[String], nextPage: Option[String])

class OlxSearchResultsScrapper() extends CommonHtmlHelpers with RichClasses with LazyLogging {
  def getResultsFrom(url: String) = {
    val doc = RJsoup.get(url).get
    parseResultsFrom(doc)
  }

  def parseOfferItem(entry: Element) = {
    val location = entry.selectSingle("span:has(i[data-icon=location-filled])").text()
    val id = entry.selectSingle("table[data-id]").attr("data-id")
    val price = validateAndParseMoney(normalizeTextToOneLine(entry.select("p.price").text()))
    val link = entry.selectSingle("td.title-cell a.linkWithHash").attr("href").stripSuffix(";promoted")
    val linkWithoutHash = if (link.count(_ == '#') == 1) link.split("#").head else link
    OlxSearchEntry(id, price, location, linkWithoutHash)
  }

  def parseResultsFrom(str: String) = {
    val doc = Jsoup.parse(str)
    doc.select("div.offer-wrapper").toVector
      .map(parseOfferItem)
      .distinctBy(_.id)
  }

  def parsePager(str: String): OlxSearchPager = {
    val doc = Jsoup.parse(str)

    val pager = doc.selectSingle("div.pager")
    if (pager != null) {
      val maxPages = pager.select("span.item").toVector.map(_.text()).filter(_.nonEmpty).map(_.toInt).max

      val prevPage = Option(pager.selectSingle("a[data-cy=page-link-prev], span[data-cy=page-link-prev]").attr("href")).filter(_.nonEmpty)
      val nextPage = Option(pager.selectSingle("a[data-cy=page-link-next], span[data-cy=page-link-next]").attr("href")).filter(_.nonEmpty)

      val currentPageNum = pager.selectSingle("span[data-cy=page-link-current]").text().toInt

      OlxSearchPager(maxPages, currentPageNum, prevPage, nextPage)
    } else OlxSearchPager(1, 1, None, None)
  }

  def parseAllResultsFor(url: String): Vector[OlxSearchEntry] = {
    assert(!url.contains("page="), "url shall not contain page param")
    val doc = RJsoup.get(url).get
    val firstPage = parseResultsFrom(doc)
    val pager = parsePager(doc)
    (firstPage ++ 2.to(pager.maxPages).flatMap { pageNum =>
      logger.info(s"Checking $pageNum page of olx")
      getResultsFrom(s"$url&page=$pageNum")
    }).distinctBy(_.id)
  }
}