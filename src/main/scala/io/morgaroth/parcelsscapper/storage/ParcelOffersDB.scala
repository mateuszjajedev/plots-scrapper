package io.morgaroth.parcelsscapper.storage

import java.util.UUID

import cats.instances.future.catsStdInstancesForFuture
import cats.syntax.flatMap._
import com.mongodb.client.model.IndexOptions
import io.morgaroth.parcelsscapper.common.{OfferRate, OfferSource}
import org.mongodb.scala._
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.{Filters, Updates}
import org.mongodb.scala.result.{DeleteResult, InsertOneResult}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Fields {
  val _id = "_id"
  val price = "price"
  val area = "area"
  val duplicateOf = "duplicateOf"
  val link = "link"
  val location = "location"
  val visited = "visited"
  val uniqueText = "uniqueText"
  val seen = "seen"
  val addedAt = "addedAt"
  val mapId = "mapId"
  val offerSource = "offerSource"
  val offerId = "offerId"
}

class ParcelOffersDB(val db: MongoCollection[ParcelOfferDbEntry]) {

  def findAll(): Future[Vector[ParcelOfferDbEntry]] = db.find().toFuture().map(_.toVector)

  def delete(id: UUID): Future[DeleteResult] = db.deleteOne(equal(Fields._id, id)).toFuture()

  def getById(id: UUID): Future[ParcelOfferDbEntry] = db.find(equal(Fields._id, id)).first().head()

  def findById(id: UUID): Future[Option[ParcelOfferDbEntry]] = db.find(equal(Fields._id, id)).first().headOption()

  def findDuplicatesOf(id: UUID): Future[Vector[ParcelOfferDbEntry]] = db.find(equal(Fields.duplicateOf, id)).toFuture().map(_.toVector)

  def updateValue(id: UUID, update: Bson): Future[ParcelOfferDbEntry] = db.findOneAndUpdate(equal(Fields._id, id), update).toFuture().map(x => println(s"$id, $update = $x")) >> getById(id)

  def updateLink(id: UUID, newValue: String): Future[ParcelOfferDbEntry] = updateValue(id, Updates.set(Fields.link, newValue))

  def updateLocation(id: UUID, location: Option[LocationCord]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate(Fields.location, location))

  def updateArea(id: UUID, newValue: Option[Long]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate(Fields.area, newValue))

  def updateMapId(id: UUID, newValue: Option[String]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate(Fields.mapId, newValue))

  def updateNote(id: UUID, newValue: Option[String]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate("note", newValue))

  def updatePlace(id: UUID, newValue: Option[String]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate("place", newValue))

  def updateUniqueText(id: UUID, newValue: Option[String]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate(Fields.uniqueText, newValue))

  def updateSeen(id: UUID, newValue: Boolean): Future[ParcelOfferDbEntry] = updateValue(id, Updates.set(Fields.seen, newValue))

  def updateVisited(id: UUID, newValue: Boolean): Future[ParcelOfferDbEntry] = updateValue(id, Updates.set(Fields.visited, newValue))

  def updateRate(id: UUID, e: OfferRate): Future[ParcelOfferDbEntry] = updateValue(id, Updates.set("rate", e))

  def updateOfferer(id: UUID, newValue: Option[String]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate("offerer", newValue))

  def updateDescription(id: UUID, old_description: String): Future[ParcelOfferDbEntry] = updateValue(id, Updates.set("description", old_description))

  def updateDuplicateOf(id: UUID, duplicateId: Option[UUID]): Future[ParcelOfferDbEntry] = updateValue(id, optionToUpdate(Fields.duplicateOf, duplicateId))

  def findFirstUnseen(offerSource: Option[OfferSource]): Future[Option[ParcelOfferDbEntry]] = {
    val q = Filters.and(equal(Fields.seen, false) :: offerSource.map(equal(Fields.offerSource, _)).toList: _*)
    db.find(q).sort(BsonDocument(Fields.addedAt -> -1)).first().toFutureOption()
  }

  private def optionToUpdate[T](field: String, value: Option[T]): Bson = {
    value.map(Updates.set(field, _)).getOrElse(Updates.unset(field))
  }

  def initialize(): Future[Unit] = {
    for {
      _ <- db.createIndex(Document(Fields.offerId -> 1), new IndexOptions().unique(true)).toFuture()
      _ <- db.createIndex(Document(Fields.seen -> 1)).toFuture()
      _ <- db.createIndex(Document(Fields.addedAt -> -1)).toFuture()
      _ <- db.createIndex(Document(Fields.mapId -> -1)).toFuture()
      _ <- db.createIndex(Document("_rawId" -> 1), new IndexOptions().unique(true)).toFuture()
    } yield ()
  }

  def findByOfferId(offerId: String, offerSource: OfferSource): Future[Option[ParcelOfferDbEntry]] = {
    if (offerId.isEmpty) Future.failed(new IllegalArgumentException("offerId is empty, shouldn't"))
    else db.find(Filters.and(Filters.equal(Fields.offerId, offerId), Filters.equal(Fields.offerSource, offerSource))).first().toFutureOption()
  }

  def store(document: ParcelOfferDbEntry): Future[InsertOneResult] =
    db.insertOne(document).toFuture()
}
