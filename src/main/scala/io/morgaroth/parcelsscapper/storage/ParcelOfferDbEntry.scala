package io.morgaroth.parcelsscapper.storage

import java.time.{LocalDate, ZoneOffset, ZonedDateTime}
import java.util.UUID

import cats.syntax.option._
import io.morgaroth.parcelsscapper.common.{AreaAI, Gumtree, Morizon, NoRateYet, OfferRate, OfferSource, OffererAI, Olx, Otodom, PlaceAI}
import io.morgaroth.parcelsscapper.scrapping.gumtree.{GumtreeFullOffer, GumtreeSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.morizon.{MorizonFullOffer, MorizonSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.olx.{OlxFullOffer, OlxSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.otodom.{OtodomFullOffer, OtodomSearchEntry}

case class ParcelOfferDbEntry(
                               _id: UUID,
                               _rawId: String,
                               offerId: String,
                               seen: Boolean,
                               rate: OfferRate,
                               link: String,
                               price: Long,
                               location: Option[LocationCord],
                               area: Option[Long],
                               mapId: Option[String],
                               note: Option[String],
                               place: Option[String],
                               offerer: Option[String],
                               addedAt: LocalDate,
                               phone: Option[String],
                               offerSource: OfferSource,
                               description: String,
                               duplicateOf: Option[UUID],
                               visited: Boolean,
                               uniqueText: Option[String],
                               _gumtreeSearchResultEntry: Option[GumtreeSearchEntry],
                               _olxSearchResultEntry: Option[OlxSearchEntry],
                               _otodomSearchResultEntry: Option[OtodomSearchEntry],
                               _morizonSearchResultEntry: Option[MorizonSearchEntry],
                               _gumtreeFullOfferEntry: Option[GumtreeFullOffer],
                               _olxFullOfferEntry: Option[OlxFullOffer],
                               _otodomFullOfferEntry: Option[OtodomFullOffer],
                               _morizonFullOfferEntry: Option[MorizonFullOffer],
                               _updatedAt: ZonedDateTime,
                               _createdAt: ZonedDateTime,
                             ) {
  lazy val pricePerMeter: Option[Long] = area.map(a => price / a)
  lazy val pricePerAr: Option[Long] = pricePerMeter.map(_ * 100)
  lazy val areaAr: Option[Double] = area.map(x => x * 1d / 100)
  lazy val shortStr: String = s"${offerSource.name}/$offerId ${areaAr.map(_.toInt).getOrElse(0)} ${price / 1000}, ${place.getOrElse("noplace")}, $link"

  lazy val areaArStr: Option[String] = areaAr.map(x => s"$x ar")
}

object ParcelOfferDbEntry {
  def apply(otodomSearchEntry: OtodomSearchEntry, otodomFullOffer: OtodomFullOffer) = {
    val id = UUID.randomUUID()
    val fullText = s"${otodomSearchEntry.title.getOrElse("")} ${otodomFullOffer.description}".trim
    new ParcelOfferDbEntry(id, id.toString,
      offerId = otodomSearchEntry.id,
      seen = false,
      rate = NoRateYet,
      link = otodomSearchEntry.link,
      price = otodomSearchEntry.price,
      location = None,
      area = otodomFullOffer.area,
      mapId = None,
      note = None,
      place = otodomFullOffer.place.orElse(PlaceAI.find(fullText)),
      offerer = otodomFullOffer.offerer.orElse(OffererAI.find(otodomFullOffer.description)),
      addedAt = otodomFullOffer.addDate,
      phone = otodomFullOffer.phone,
      offerSource = Otodom,
      description = fullText,
      None, false, None,
      None, None, Some(otodomSearchEntry), None,
      None, None, Some(otodomFullOffer), None,
      ZonedDateTime.now(ZoneOffset.UTC), ZonedDateTime.now(ZoneOffset.UTC)
    )
  }

  def apply(gumtreeSearchEntry: GumtreeSearchEntry, gumtreeFullOffer: GumtreeFullOffer): ParcelOfferDbEntry = {
    val id = UUID.randomUUID()
    val fullText = s"${gumtreeFullOffer.title.getOrElse("")} ${gumtreeFullOffer.description}".trim
    new ParcelOfferDbEntry(id, id.toString,
      offerId = gumtreeSearchEntry.id,
      seen = false,
      rate = NoRateYet,
      link = gumtreeSearchEntry.link,
      price = gumtreeSearchEntry.price,
      location = None,
      area = AreaAI.find(fullText).orElse(gumtreeFullOffer.title.flatMap(AreaAI.find)),
      mapId = None,
      note = None,
      place = PlaceAI.find(fullText),
      offerer = OffererAI.find(gumtreeFullOffer.description),
      addedAt = gumtreeFullOffer.addDate,
      phone = gumtreeFullOffer.phone,
      offerSource = Gumtree,
      description = fullText,
      None, false, None,
      Some(gumtreeSearchEntry), None, None, None,
      Some(gumtreeFullOffer), None, None, None,
      ZonedDateTime.now(ZoneOffset.UTC), ZonedDateTime.now(ZoneOffset.UTC)
    )
  }

  def apply(olxSearchEntry: OlxSearchEntry, olxFullOffer: OlxFullOffer): ParcelOfferDbEntry = {
    val id = UUID.randomUUID()
    val fullText = s"${olxFullOffer.title} ${olxFullOffer.description}"
    new ParcelOfferDbEntry(id, id.toString,
      offerId = olxSearchEntry.id,
      seen = false,
      rate = NoRateYet,
      link = olxSearchEntry.link,
      price = olxSearchEntry.price,
      location = None,
      area = olxFullOffer.area.orElse(AreaAI.find(fullText)),
      mapId = None,
      note = None,
      place = PlaceAI.find(fullText),
      offerer = OffererAI.find(olxFullOffer.description),
      addedAt = olxFullOffer.addDate,
      phone = olxFullOffer.phone,
      offerSource = Olx,
      description = fullText,
      None, false, None,
      None, Some(olxSearchEntry), None, None,
      None, Some(olxFullOffer), None, None,
      ZonedDateTime.now(ZoneOffset.UTC), ZonedDateTime.now(ZoneOffset.UTC)
    )
  }

  def apply(searchEntry: MorizonSearchEntry, fullOffer: MorizonFullOffer): ParcelOfferDbEntry = {
    val id = UUID.randomUUID()
    val fullText = fullOffer.description
    new ParcelOfferDbEntry(id, id.toString,
      offerId = searchEntry.id,
      seen = false,
      rate = NoRateYet,
      link = searchEntry.link,
      price = searchEntry.price,
      location = None,
      area = fullOffer.area.some,
      mapId = None,
      note = None,
      place = fullOffer.place.some,
      offerer = OffererAI.find(fullOffer.description),
      addedAt = fullOffer.addDate,
      phone = fullOffer.phone.some,
      offerSource = Morizon,
      description = fullText,
      None, false, None,
      None, None, None, Some(searchEntry),
      None, None, None, Some(fullOffer),
      ZonedDateTime.now(ZoneOffset.UTC), ZonedDateTime.now(ZoneOffset.UTC)
    )
  }
}
