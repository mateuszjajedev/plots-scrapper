package io.morgaroth.parcelsscapper.boot

import cats.instances.future.catsStdInstancesForFuture
import cats.instances.vector._
import cats.syntax.traverse._
import io.morgaroth.parcelsscapper.common.{AreaAI, OffererAI, PlaceAI}
import io.morgaroth.parcelsscapper.storage.MongoRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


object AIUpdater {
  def main(args: Array[String]): Unit = {
    val genericDb = MongoRepository.GenericOffersDB()

    val work = genericDb.findAll().flatMap(_.traverse { entry =>
      Vector(
        if (entry.place.isEmpty) {
          val newPlace = PlaceAI.find(entry.description)
          newPlace.map(Some(_)).map(genericDb.updatePlace(entry._id, _)).getOrElse(Future.successful(1))
        } else Future.successful(1),
        if (entry.area.isEmpty) {
          val newPlace = AreaAI.find(entry.description).orElse(entry._gumtreeFullOfferEntry.flatMap(_.title).flatMap(AreaAI.find))
          newPlace.map(Some(_)).map(genericDb.updateArea(entry._id, _)).getOrElse(Future.successful(1))
        } else Future.successful(1),
        if (entry.offerer.isEmpty) {
          val newPlace = OffererAI.find(entry.description)
          newPlace.map(Some(_)).map(genericDb.updateOfferer(entry._id, _)).getOrElse(Future.successful(1))
        } else Future.successful(1),
      ).sequence
    })

    Await.result(work, 5.minutes)
  }
}
