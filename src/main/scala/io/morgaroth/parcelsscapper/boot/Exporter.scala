package io.morgaroth.parcelsscapper.boot

import io.morgaroth.parcelsscapper.common.{FutureMain, Highway, JustNo}
import io.morgaroth.parcelsscapper.storage.{LocationCord, MongoRepository, ParcelOfferDbEntry}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Exporter extends FutureMain {
  def fmain(args: List[String]): Future[Int] = {
    val genericDb = MongoRepository.GenericOffersDB()
    for {
      all <- genericDb.findAll()
      toVisitLocalized = all.filter(e => e.location.isDefined && !e.visited && e.duplicateOf.isEmpty && e.rate != JustNo && e.seen && e.rate != Highway)
      unique = toVisitLocalized.groupBy(_.location.get)
      _ = unique.foreach(render)
      _ = println(s"${toVisitLocalized.length} ofert")
    } yield 1
  }

  def render(e: (LocationCord, Vector[ParcelOfferDbEntry])) = {
    val biggest = e._2.maxBy(_.area.get)
    println(s"#MapId ${biggest.mapId.getOrElse("WTF")}")
    println(s"${biggest.areaArStr.getOrElse("WTF")}, ${biggest.price / 1000}k, ${biggest.pricePerAr.map(_ / 1000).map(x => s"${x}zł/a").getOrElse("WTF")}, ${biggest.place.getOrElse("noplace")}")
    println(s"mapa: ${e._1.toGoogleMapsLink}")
    println(s"oferta: ${biggest.link}")
    //    e._2.foreach(x => println(s"oferta: ${x.link}"))
    println("")
  }
}
