package io.morgaroth.parcelsscapper.errors

sealed abstract class ScrapperError(message: String) extends Throwable(message)

case class ParsingAssertion(hint: String) extends ScrapperError(s"Parsing exception: $hint")

case class ScrappingException(htmlDoc: String, e: Throwable) extends ScrapperError(s"ScrappingException: ${e.getMessage}")
