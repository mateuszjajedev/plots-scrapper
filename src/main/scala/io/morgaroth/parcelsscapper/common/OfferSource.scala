package io.morgaroth.parcelsscapper.common

sealed abstract class OfferSource(val name: String) extends Product with Serializable

case object Otodom extends OfferSource("otodom")

case object Gumtree extends OfferSource("gumtree")

case object Olx extends OfferSource("olx")

case object Morizon extends OfferSource("morizon")

object OfferSource {
  val values = Seq(Otodom, Gumtree, Olx, Morizon)
  val valuesClasses = values.map(_.getClass)
  val byName = values.map(x => x.name -> x).toMap
}