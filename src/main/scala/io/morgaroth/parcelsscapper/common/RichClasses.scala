package io.morgaroth.parcelsscapper.common

import io.morgaroth.parcelsscapper.errors.ParsingAssertion
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.FiniteDuration
import scala.jdk.CollectionConverters._
import cats.syntax.either._

trait RichClasses {

  implicit class RichElement(e: Element) {
    def selectSingle(query: String): Element = e.select(query).assertSingle(query)
  }

  implicit class RichElements(e: Elements) {
    def assertSingle(hint: String = null): Element = {
      if (e.size() > 1) {
        throw ParsingAssertion(Option(hint).getOrElse("no hint"))
      }
      e.first()
    }

    def selectSingle(query: String): Element = e.select(query).assertSingle(query)

    def toVector: Vector[Element] = e.asScala.toVector
  }

  implicit class RichFuture[T](v: Future[T]) {
    def futureValue(timeout: FiniteDuration)(implicit ex: ExecutionContext): Either[Throwable, T] = {
      Await.result(v.map(_.asRight[Throwable]).recover(_.asLeft), timeout)
    }
  }

}
