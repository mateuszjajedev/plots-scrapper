package io.morgaroth.parcelsscapper.common

object Geoportal {

  val data = Map(
    "Wola Batorska" -> "https://geoportal360.pl/12/wielicki/niepolomice-121904/5/0007-wola-batorska",
    "Golkowice" -> "https://geoportal360.pl/12/wielicki/wieliczka-121905/5/0006-golkowice",
    "Wieliczka" -> "https://geoportal360.pl/12/wielicki/wieliczka-121905/",
    "Kobylec" -> "https://geoportal360.pl/12/bochenski/lapanow-120105/2/0008-kobylec",
    "Nieszkowice Małe" -> "https://geoportal360.pl/12/bochenski/bochnia-120102/2/0020-nieszkowice-male",
  )

  def resolve(place: String): Option[String] = data.get(place.split(",").head)
}
