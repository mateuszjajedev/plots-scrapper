package io.morgaroth.parcelsscapper.common

import java.io.IOException
import java.net.{SocketTimeoutException, URLDecoder}

import org.jsoup.{HttpStatusException, Jsoup}

import scala.util.{Success, Try}

object RJsoup {
  def get(url: String, retries: Int = 5): Try[String] = {
    println(s"Requesting: $url")
    Try(Jsoup.connect(URLDecoder.decode(url, "utf-8")).followRedirects(true).execute().body())
      .flatMap {
        case "" => get(url, retries - 1)
        case normalContent => Success(normalContent)
      }
      .recoverWith {
        case e: HttpStatusException if e.getStatusCode >= 500 && e.getStatusCode < 600 && retries > 0 =>
          get(url, retries - 1)
        case e: SocketTimeoutException if retries > 0 =>
          get(url, retries - 1)
        case _: IOException if retries > 0 =>
          get(url, retries - 1)
      }
  }
}
