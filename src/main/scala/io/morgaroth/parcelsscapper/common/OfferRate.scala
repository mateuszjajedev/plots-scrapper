package io.morgaroth.parcelsscapper.common

sealed abstract class OfferRate(val name: String, val humanName: String) extends Product with Serializable

case object NoRateYet extends OfferRate("no-rate", "Bez Oceny")

case object NotRelevant extends OfferRate("not-relevant", "Nie Istotne")

case object TooExpensive extends OfferRate("too-expensive", "Za droga")

case object NonHomePlace extends OfferRate("non-home-place", "Nie Mieszkaniowa")

case object JustNo extends OfferRate("no", "Nie")

case object MayVisit extends OfferRate("may-visit", "Można zobaczyć")

case object Maybe extends OfferRate("maybe", "Może")

case object Duplicate extends OfferRate("duplicate", "Duplikat")

case object Later extends OfferRate("later", "Na później")

case object GoodOne extends OfferRate("good-one", "OK")

case object Highway extends OfferRate("highway", "Autostrada")

object OfferRate {
  val values = Seq(NoRateYet, JustNo, TooExpensive, NotRelevant, Duplicate, Later, MayVisit, Maybe, GoodOne, Highway, NonHomePlace)
  val valuesClasses = values.map(_.getClass)

  val byName = values.map(x => x.name -> x).toMap
  val byHumanName = values.map(x => x.humanName -> x).toMap

  def indexOf(rate: OfferRate): Int = values.indexOf(rate)
}