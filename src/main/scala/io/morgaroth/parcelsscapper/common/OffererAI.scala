package io.morgaroth.parcelsscapper.common

object OffererAI {
  private val markerStrings: List[(String, String)] = List(
    List("ALOKUM Nieruchomości") -> "Alokum",
    List("AN-Optima") -> "AN Optima",
    List("Biuro Nieruchomości ARCHIK") -> "ARCHIK",
    List("aston.com.pl") -> "Aston",
    List("CONSILO Finanse i Nieruchomości") -> "Consilo",
    List("Emmerson Lumico") -> "Emmerson",
    List() -> "Danax",
    List("GET.PL Nieruchomości") -> "GET.PL",
    List("WARTO kupić z GRUPĄ B12") -> "Grupa B12",
    List("Nieruchomościowa Grupa Inwestycyjna") -> "Grupa Inwestycyjna",
    List("Jarczewski Nieruchomości") -> "Jarczewski",
    List("Legal Nieruchomości") -> "Legal",
    List("MMINWEST") -> "MM Invest",
    List("M3 Nieruchomości") -> "M3",
    List("nieruchomosciaz.pl") -> "NieruchomościAZ",
    List("www.n-b-n.pl") -> "Nowina Borkowski",
    List("nowodworskiestates.pl") -> "Nowodworski Estates",
    List("Biuro Nieruchomości Petryla") -> "Petryla",
    List("Piotrowski nieruchomości") -> "Piotrowski",
    List("Plan A - Nieruchomości") -> "Plan A",
    List("Prosper Nieruchomości") -> "Prosper",
    List("sadurscy.pl") -> "Sadurscy",
    List("Nieruchomości STROJNA") -> "Strojna",
    List("Nieruchomości Syldoos") -> "Syldoos",
    List("PÓŁNOC Nieruchomości") -> "Północ nieruchomości",
  ).flatMap(x => (x._2 :: x._1).map(_.toLowerCase).map(y => y -> x._2))

  def find(description: String): Option[String] = {
    val lowerCased = description.toLowerCase
    markerStrings.find(x => lowerCased.contains(x._1)).map(_._2)
  }

  def normalize(agencyName: String) = find(agencyName).getOrElse(agencyName)
}