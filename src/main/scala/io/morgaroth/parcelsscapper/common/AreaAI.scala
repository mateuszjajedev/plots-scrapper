package io.morgaroth.parcelsscapper.common

import cats.syntax.option._
import com.typesafe.scalalogging.LazyLogging

object AreaAI extends LazyLogging {
  val haRegexes = List(
    """(\d+([,.]\d+)?) ?ha""".r
  )
  val arRegexes = List(
    """(\d+(\h*[,.]\h*\d+)?)\h*?aró?[. ]""".r,
    """(\d+(\h*[,.]\h*\d+)?)\h*?a[. ]""".r,
    """(\d+)\h*?arów[. ]""".r,
    """(\d+)[\h-]*?arowa[. ]""".r,
  )
  val mkwRegexes = List(
    """ (\d+)([,.]\d+)? ?m ?2[. /]""".r,
    """^(\d+)([,.]\d+)? ?m ?2[. /]""".r,
  )
  val sizeRegexes = List(
    """^(\d+)mx(\d+)m$""".r,
  )

  private val regexesToRemove = Vector(
    "Forteczna 51a",
    """z czego około \d+ ar stanowi""",
    "a plot of land for sale",
  ).map(_.toLowerCase)

  private def normalizeNumber(in: String) = in.replaceAll("""\h+""", "").replaceAll(",", ".")

  def find(description: String): Option[Long] = {
    val lowerCased = regexesToRemove.foldLeft(description.toLowerCase) {
      case (state, regex) => state.replaceAll(regex, "")
    }
    val has = haRegexes.flatMap { reg => reg.findAllMatchIn(lowerCased).map(_.group(1)).map(normalizeNumber).map(x => BigDecimal(x) * 10000).map(_.toLong).toVector }
    val ars = arRegexes.flatMap { reg => reg.findAllMatchIn(lowerCased).map(_.group(1)).map(normalizeNumber).map(x => BigDecimal(x) * 100).map(_.toLong).toVector }
    val mkw = mkwRegexes.flatMap { reg => reg.findAllMatchIn(lowerCased).map(_.group(1).toLong).toVector }
    val size = sizeRegexes.flatMap { reg => reg.findAllMatchIn(lowerCased).map { matcher => matcher.group(1).toLong * matcher.group(2).toLong }.toVector }
    val allSizes = (ars ++ mkw ++ has ++ size).distinct
    if (allSizes.length <= 1) allSizes.headOption else none
  }
}
object NormalizedArea {

  import cats.syntax.option._

  private val extractors = List(
    """^(\d+(\.\d+)?)ha$""".r -> 10000,
    """^(\d+(\.\d+)?)ar?$""".r -> 100,
    """^(\d+(\.\d+)?)m[2²]$""".r -> 1,
  )

  def unapply(input: String): Option[Long] = {
    val normalized = input.replaceAll("""\h+""", "").replaceAll(",", ".")
    extractors.foldLeft(none[Long]) {
      case (acc, (ex, m)) => acc.orElse {
        ex.findFirstMatchIn(normalized).map(x => BigDecimal(x.group(1)) * m).map(_.toLong)
      }
    }.orElse {
      val r = """^(\d+)mx(\d+)m$""".r
      if (r.matches(normalized)) {
        r.findFirstMatchIn(normalized).map { matcher =>
          matcher.group(1).toLong * matcher.group(2).toLong
        }
      } else none
    }
  }
}
