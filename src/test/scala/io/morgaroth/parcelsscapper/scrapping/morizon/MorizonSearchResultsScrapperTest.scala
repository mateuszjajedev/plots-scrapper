package io.morgaroth.parcelsscapper.scrapping.morizon

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MorizonSearchResultsScrapperTest extends AnyFlatSpec with ResourcesLoader with Matchers {
  val underTest = new MorizonSearchResultsScrapper()

  behavior of "MorizonSearchResultsScrapper"

  it should "parse results list correctly" in {
    val results = underTest.parseResultsFrom(loadRes("morizon/morizon_search_result_1.html"))
    results should have size 21
    results.head shouldBe MorizonSearchEntry("1mzn2035859085", 210000, "https://www.morizon.pl/oferta/sprzedaz-dzialka-wielicki-wieliczka-1000m2-mzn2035859085")
  }

  it should "parse results list correctly 2" in {
    val results = underTest.parseResultsFrom(loadRes("morizon/morizon_search_result_2.html"))
    results should have size 35
    results.head shouldBe MorizonSearchEntry("1mzn2036235313", 117000, "https://www.morizon.pl/oferta/sprzedaz-dzialka-wielicki-klaj-wiejska-1500m2-mzn2036235313")
  }

  it should "parse pager from page where it is not" in {
    val results = underTest.parsePager(loadRes("morizon/morizon_search_result_1.html"))
    results shouldBe MorizonSearchPager(1)
  }

  it should "parse pager from page where it is" in {
    val results = underTest.parsePager(loadRes("morizon/morizon_search_result_2.html"))
    results shouldBe MorizonSearchPager(18)
  }
}
