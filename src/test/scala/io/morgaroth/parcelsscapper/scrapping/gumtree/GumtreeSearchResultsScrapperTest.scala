package io.morgaroth.parcelsscapper.scrapping.gumtree

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.io.Source

class GumtreeSearchResultsScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {

  val underTest = new GumtreeSearchResultsScrapper()

  behavior of "GumtreeSearchResultsScrapper"

  it should "parse results list correctly" in {
    val results = underTest.parseResultsFrom(Source.fromResource("gumtree/gumtree_search_result_1.html").mkString)
    results should have size 20
    results.head shouldBe GumtreeSearchEntry("1001310075410910477144109", "131007541", 22400, "https://www.gumtree.pl/a-dzialki/wieliczka/dzialka-28-ar-kozmice-male-pow-wieliczka/1001310075410910477144109")
  }

  it should "parse pager well when on a first page" in {
    underTest.parsePager(Source.fromResource("gumtree/gumtree_search_result_1.html").mkString) shouldBe GumtreeSearchPager(26, 1, None, Some("https://www.gumtree.pl/s-dzialki/wieliczka/page-2/v1c9194l3200224p2"))
  }

  it should "parse pager well when on a second page" in {
    underTest.parsePager(Source.fromResource("gumtree/gumtree_search_result_page2_1.html").mkString) shouldBe GumtreeSearchPager(26, 2, Some("https://www.gumtree.pl/s-dzialki/wieliczka/v1c9194l3200224p1"), Some("https://www.gumtree.pl/s-dzialki/wieliczka/page-3/v1c9194l3200224p3"))
  }

  it should "parse pager well when inside results" in {
    underTest.parsePager(Source.fromResource("gumtree/gumtree_search_result_page5_1.html").mkString) shouldBe GumtreeSearchPager(26, 5, Some("https://www.gumtree.pl/s-dzialki/wieliczka/page-4/v1c9194l3200224p4"), Some("https://www.gumtree.pl/s-dzialki/wieliczka/page-6/v1c9194l3200224p6"))
  }

  it should "parse pager well when last page of results" in {
    underTest.parsePager(loadRes("gumtree/gumtree_search_result_last_page_1.html")) shouldBe GumtreeSearchPager(25, 25, Some("https://www.gumtree.pl/s-dzialki/wieliczka/page-24/v1c9194l3200224p24?priceType=FIXED"), None)
  }

  it should "parse when there is no pager" in {
    underTest.parsePager(loadRes("gumtree/gumtree_search_result_no_pager.html")) shouldBe GumtreeSearchPager(1, 1, None, None)
  }
}

