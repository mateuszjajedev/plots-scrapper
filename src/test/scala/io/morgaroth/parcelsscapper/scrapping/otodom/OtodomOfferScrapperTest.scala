package io.morgaroth.parcelsscapper.scrapping.otodom

import java.time.LocalDate

import io.morgaroth.parcelsscapper.common.{NonActualOffer, OfferParsed}
import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OtodomOfferScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {

  behavior of "OtodomOfferScrapper"

  val underTest = new OtodomOfferScrapper()

  it should "parse offer page" in {
    underTest.readServerAppData(loadRes("otodom/example_server_app_data.json")) shouldBe
      (List("+48602150756", "+48123572757", "+48602150721"), Some("GRUPA B12"), "Siercza, Siercza, wielicki, małopolskie", LocalDate.of(2020, 5, 7), 2160)

    val expectedDate = LocalDate.of(2020, 5, 7)
    underTest.parseOffer(loadRes("otodom/otodom_full_offer_1.html")) should matchPattern {
      case OfferParsed(OtodomFullOffer(Some("+48602150756,+48123572757,+48602150721"), Some("Grupa B12"), _, Some("Siercza"), `expectedDate`, Some(2160))) =>
    }
  }

  it should "parse offer page2" in {
    val d = LocalDate.of(2020, 3, 19)
    underTest.parseOffer(loadRes("otodom/otodom_full_offer_2.html")) should matchPattern {
      case OfferParsed(OtodomFullOffer(Some("+48505975843"), Some("Nieruchomości A-Z"), _, Some("Grajów"), `d`, Some(1530))) =>
    }
  }

  it should "handle outdated offer" in {
    underTest.parseOffer(loadRes("otodom/otodom_outdated_offer_1.html")) shouldBe NonActualOffer()
  }

  it should "parse offer example 3" in {
    val d = LocalDate.of(2020, 2, 3)
    underTest.parseOffer(loadRes("otodom/otodom_full_offer_3.html")) should matchPattern {
      case OfferParsed(OtodomFullOffer(Some("+48512285686,+48509355636"), Some("Strojna"), _, Some("Wieliczka, Krzyszkowice"), `d`, Some(1077))) =>
    }
  }
}
