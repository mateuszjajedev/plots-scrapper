package io.morgaroth.parcelsscapper.scrapping.otodom

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec

import scala.io.Source

class OtodomSearchResultsScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader{

  val underTest = new OtodomSearchResultsScrapper()

  behavior of "OtodomSearchResultsScrapper"

  it should "parse results list correctly" in {
    val results = underTest.parseResultsFrom(loadRes("otodom_search_result_1.html"))
    results should have size 27
  }

  it should "parse pager" in {
    underTest.parsePager(loadRes("otodom_search_result_1.html")) shouldBe OtodomSearchPager(24, 20)
  }
}
